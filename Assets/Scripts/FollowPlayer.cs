using UnityEngine;

public class FollowPlayer : MonoBehaviour
{

    public Transform player; 
    public float smoothSpeed = 0.125f; // Smoothing factor
    public Vector3 offset;

    private void LateUpdate()
    {
        Vector3 desiredPosition = player.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
        transform.position = smoothedPosition;

        transform.LookAt(player.position); // Camera looks at the target
    }
}
