using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MagnetMoveX : MonoBehaviour
{
    [SerializeField] private float duration = 2.0f;
    [SerializeField] private Vector3 target;

    private void Start()
    {
        transform.DOMove(target, duration)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Yoyo);
    }
}
