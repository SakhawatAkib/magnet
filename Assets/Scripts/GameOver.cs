﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Animator animator;
    private static readonly int FadeOut = Animator.StringToHash("FadeOut");
    [SerializeField] private PlayerController player;
    [SerializeField] private SwipeInput swipeInput;

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "Player")
        {
            animator.SetTrigger(FadeOut);
            StartCoroutine(LoadScene());
        }
    }

    private IEnumerator LoadScene()
    {
        player.enabled = false;
        swipeInput.enabled = false;
        yield return new WaitForSeconds(0.5f);
        DOTween.KillAll();
        SceneManager.LoadScene(0);
    }
}
