using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class BlueMagnetBehavior : MonoBehaviour
{
    public Material material;
    public float radius = 4f;
    public float pullSpeed;
    private readonly Color blue = Color.blue;
    [SerializeField] private PlayerController playerController;

    private void Update()
    {
        Vector3 rayDirection = transform.position - playerController.transform.position;
        if(rayDirection.magnitude > radius) return;
        float strength = rayDirection.magnitude.Remap(0,radius, 1, 0);
        if (Physics.Raycast(playerController.transform.position, rayDirection, out RaycastHit  hit))
        {
            Vector3 movdirection = hit.point - playerController.transform.position;
            movdirection = new Vector3(movdirection.x, 0, movdirection.z);
            if (material.color == blue)
            {
                playerController.isMagnet = false;
                playerController.controller.Move(-movdirection * (Time.deltaTime * pullSpeed * strength));
            }
            else
            {
                playerController.isMagnet = true;
                playerController.controller.Move(movdirection * (Time.deltaTime * pullSpeed * strength));
            }
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.transform.CompareTag("Player"))
        {
            playerController.controller.enabled = false;
            playerController.transform.LookAt(transform);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (other.transform.CompareTag("Player"))
        {
            playerController.controller.enabled = true;
        }
    }

    /*float GetObjectRadius(Transform obj)
    {
        Vector3 localScale = obj.localScale;
        float averageScale = (localScale.x + localScale.y + localScale.z) / 3f;
        return averageScale / 2f;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = blue;
        float angleIncrement = 360.0f / 64;

        Vector3 prevPoint = Vector3.zero;
        for (int i = 0; i <= 64; i++)
        {
            float angle = i * angleIncrement;
            float x = radius * Mathf.Cos(Mathf.Deg2Rad * angle);
            float z = radius * Mathf.Sin(Mathf.Deg2Rad * angle);
            Vector3 currentPoint = transform.position + new Vector3(x, 0, z);

            if (i > 0)
            {
                Gizmos.DrawLine(prevPoint, currentPoint);
            }

            prevPoint = currentPoint;
        }
    }*/
}
