using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    public float sprintSpeed = 10f; // Speed when double-tapping
    public float jumpForce = 7f;
    public float gravity = -9.81f;
    public float rotationSpeed = 3;
    public Transform groundCheck;
    public LayerMask groundLayer;
    public bool isMagnet;

    public CharacterController controller;
    private Vector3 playerVelocity;
    private bool isGrounded;
    public Joystick joystick;
    public Material material;
    
    private bool isSprinting = false;
    private float lastTapTime = 0f;
    private float maxTimeBetweenTaps = 0.5f;
    
    private readonly Color red = Color.red;
    private readonly Color blue = Color.blue;

    private void Awake()
    {
        StartCoroutine(WaitAndChangeColor());
    }

    private void Update()
    {
        // Check if the character is grounded.
        isGrounded = controller.isGrounded;

        // Movement using joystick.
        float horizontalInput = joystick.Horizontal;
        float verticalInput = joystick.Vertical;
        Vector3 moveDirection = new Vector3(horizontalInput, 0, verticalInput);

        if (isGrounded)
        {
            if (Time.time - lastTapTime <= maxTimeBetweenTaps && !isMagnet)
            {
                isSprinting = true;
                moveDirection *= sprintSpeed;
            }
            else
            {
                isSprinting = false;
                moveDirection *= moveSpeed;
            }
            // Jumping.
            if (Input.GetButtonDown("Jump")) Jump();
        }
        else moveDirection *= isSprinting ? sprintSpeed : moveSpeed;
        
        
        // Apply gravity to simulate continuous falling.
        playerVelocity.y += gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime + playerVelocity * Time.deltaTime);
        
        // Ensure the character stays grounded.
        if (isGrounded && playerVelocity.y < 0)playerVelocity.y = -2f;
        
        if (!(moveDirection.magnitude > 0)) return;
        Quaternion targetRotation = Quaternion.LookRotation(moveDirection, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
    }

    public void Jump()
    {
        if (controller.isGrounded)
            playerVelocity.y = Mathf.Sqrt(jumpForce * -2f * gravity);
    }
    
    private IEnumerator WaitAndChangeColor()
    {
        bool isBlue = true;
        while (true)
        {
            isBlue = !isBlue;
            Color[] colors = { red, blue };
            yield return new WaitForSeconds(Random.Range(3, 5));
            material.color = colors[Convert.ToInt16(value: isBlue)];
        }
        // ReSharper disable once IteratorNeverReturns
    }
    
    public void RecordTapTime()
    {
        lastTapTime = Time.time;
    }
}