using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeInput : MonoBehaviour
{
    public RectTransform joystickTouchArea; // Reference to the joystick touch area's RectTransform.
    public PlayerController playerController;
    private Vector2 touchStartPos;
    private bool isSwipe;

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            foreach (Touch touch in Input.touches)
            {
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        touchStartPos = touch.position;
                        isSwipe = true;
                        break;
                    case TouchPhase.Moved:
                        if (Mathf.Abs(touch.position.y - touchStartPos.y) < 10f)
                            isSwipe = false;
                        break;
                    case TouchPhase.Ended:
                        if (isSwipe && touch.position.y > touchStartPos.y)
                        {
                            // jump action here.
                            playerController.Jump();
                        }
                        else if (isSwipe)
                        {
                            // Record tap time for double-tap detection.
                            playerController.RecordTapTime();
                        }
                        break;
                }
                
            }
        }
    }

    private bool IsWithinJoystickTouchArea(Vector2 touchPosition)
    {
        if (joystickTouchArea == null)
            return false;

        Vector2 localPoint;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(joystickTouchArea, touchPosition, null, out localPoint);
        return joystickTouchArea.rect.Contains(localPoint);
    }
}